package impls;

import CountMap.CountMap;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CountMapImpl<T> implements CountMap<T> {

    private int size;
    private final List<Entry<T>> entries;

    public CountMapImpl() {
        entries = new ArrayList<>();
    }

    public CountMapImpl(int initialCapacity){
        entries = new ArrayList<>(initialCapacity);
    }

    @Override
    public void add(T t) {
        Entry<T> entry = new Entry<>(t);
        if (contains(t))
            entries.get(entries.indexOf(entry)).count++;
        else
            entries.add(entry);
        size++;
    }

    @Override
    public boolean contains(T t) {
        return entries.contains(new Entry<>(t));
    }

    @Override
    public int getCount(T t) {
        return contains(t) ? entries.get(entries.indexOf(new Entry<>(t))).count : 0;
    }

    @Override
    public int removeOne(T t) {
        if (!contains(t)) return 0;
        int count = getCount(t);
        Entry<T> entry = new Entry<>(t);
        if (count <= 1) {
            entries.remove(entry);
            return count;
        }
        entries.get(entries.indexOf(entry)).count--;
        size--;
        return count;
    }

    @Override
    public Stream<T> stream() {
        return entries.stream().map(o -> o.value);
    }

    @Override
    public int removeAll(T t) {
        if(!contains(t))return 0;
        int count = getCount(t);
        Entry<T> entry = new Entry<>(t);
        size -= entries.get(entries.indexOf(entry)).count;
        entries.remove(entry);
        return count;
    }

    /**
     * Size of all added elements
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Size of unique elements
     */
    public int distinctCount(){
        return entries.size();
    }

    @Override
    public Map<T, Integer> toMap() {
        return entries.stream().collect(Collectors.toMap(
                o -> o.value,
                o -> o.count));
    }

    @Override
    public void toMap(Map<T, Integer> dest) {
        for (Entry<T> e :
                entries)
            dest.put(e.value, dest.getOrDefault(e.value, 0) + e.count);
    }

    @Override
    public void addAll(CountMap<T> source) {
        for (T t:
             source) {
            if(contains(t))
                entries.get(entries.indexOf(new Entry<>(t))).count++;
            else
                entries.add(new Entry<>(t));
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<>() {
            int index = 0;

            @Override
            public boolean hasNext() {
                return index < entries.size();
            }

            @Override
            public T next() {
                return entries.get(index++).value;
            }
        };
    }

    @Override
    public String toString() {
        return "CountMapImpl{" +
                "size=" + size +
                ", entries=" + entries +
                '}';
    }

    protected static class Entry<T> {

        private final T value;
        private int count;

        public Entry(T value) {
            this.value = value;
            count = 1;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Entry<?> entry = (Entry<?>) o;
            return Objects.equals(value, entry.value);
        }

        @Override
        public int hashCode() {
            return Objects.hash(value);
        }

        @Override
        public String toString() {
            return "Entry{" +
                    "value=" + value +
                    ", count=" + count +
                    '}';
        }
    }
}
