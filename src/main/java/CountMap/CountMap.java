package CountMap;

import java.util.Map;
import java.util.stream.Stream;

public interface CountMap<T> extends Iterable<T>{
    void add(T t);

    int getCount(T t);

    int removeOne(T t);

    int removeAll(T t);

    int size();

    void addAll(CountMap<T> source);

    Map<T, Integer> toMap();

    void toMap(Map<T, Integer> dest);

    boolean contains(T t);

    Stream<T> stream();
}
