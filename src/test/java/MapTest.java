import CountMap.CountMap;
import impls.CountMapImpl;

import java.util.HashMap;
import java.util.Map;

public class MapTest {
    public static void main(String[] args) {
        CountMap<String> strings = new CountMapImpl<>();
        strings.add("Hello");
        strings.add("Hello");
        strings.add("Hello_");
        Map<String, Integer> map = new HashMap<>();
        map.put("Hello", 1);
        strings.toMap(map);
        System.out.println(map);
        map.computeIfPresent("Hello", (o1, o2) -> o1.length() + o2);
        System.out.println(map);
    }
}
